README
======
The osmobiclient modules helps you creating a mobile optimized website. 
In order to use the module, an account must be created at http://www.osmobi.com.

For more information, documentation and support,
go to http://www.osmobi.com or the support pages at http://osmobi.zendesk.com